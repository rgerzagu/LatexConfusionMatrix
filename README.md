# LatexConfusionMatrix


This small package display a Table in Latex format based on a confusion matrix sotred in a .csv file


Assuming a CSV table in a file `confusionMatrix.csv`

>71;28;0.2;0.4;0.4  
>0;91;8;0.5;0.5  
>5;7;85;3;0  
>1;1;4;90;4  
>0;2;25;8;65  


A Latex `confusionMatrix.tex` file can be generated with the following command 

```
# --- Create the header
header      = (["Tx\$_$i\$" for i = 1 : 1 : 5])
row_labels  = (["Tx\$_$i\$" for i = 1 : 1 : 5])
row_name_column_title = "\\backslashbox[15mm][]{\\footnotesize True}{\\footnotesize Guess}"

m = latexConfusionMatrix("test/confusionMatrix.csv";
    doSave=true,
    standalone=true,
    header,
    row_name_column_title,
    row_labels)
```


Background of the cell is colored based on the score: with a green range for the score in the diagonal (darker green for best scores) and in a gray scale for the other locations in the matrix (corresponding to errors)


   It generates a new tex file with the following content 
```
\documentclass[landscape]{standalone}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{xcolor}
\usepackage{colortbl}
%\usepackage[landscape]{geometry}

\begin{document}
\begin{tabular}{rccccc}
  \hline
   & Tx$_1$ & Tx$_2$ & Tx$_3$ & Tx$_4$ & Tx$_5$ \\\hline
  Tx$_1$ & \cellcolor[rgb]{0.757,0.953,0.678}{\textcolor{black}{71.0}} & \cellcolor[rgb]{0.498,0.498,0.498}{\textcolor{black}{28.0}} & \cellcolor[rgb]{1.0,1.0,1.0}{\textcolor{black}{0.2}} & \cellcolor[rgb]{1.0,1.0,1.0}{\textcolor{black}{0.4}} & \cellcolor[rgb]{1.0,1.0,1.0}{\textcolor{black}{0.4}} \\
  Tx$_2$ & \cellcolor[rgb]{1.0,1.0,1.0}{\textcolor{black}{0.0}} & \cellcolor[rgb]{0.443,0.765,0.4}{\textcolor{black}{91.0}} & \cellcolor[rgb]{0.89,0.89,0.89}{\textcolor{black}{8.0}} & \cellcolor[rgb]{1.0,1.0,1.0}{\textcolor{black}{0.5}} & \cellcolor[rgb]{1.0,1.0,1.0}{\textcolor{black}{0.5}} \\
  Tx$_3$ & \cellcolor[rgb]{0.929,0.929,0.929}{\textcolor{black}{5.0}} & \cellcolor[rgb]{0.89,0.89,0.89}{\textcolor{black}{7.0}} & \cellcolor[rgb]{0.549,0.839,0.478}{\textcolor{black}{85.0}} & \cellcolor[rgb]{0.969,0.969,0.969}{\textcolor{black}{3.0}} & \cellcolor[rgb]{1.0,1.0,1.0}{\textcolor{black}{0.0}} \\
  Tx$_4$ & \cellcolor[rgb]{0.984,0.984,0.984}{\textcolor{black}{1.0}} & \cellcolor[rgb]{0.984,0.984,0.984}{\textcolor{black}{1.0}} & \cellcolor[rgb]{0.949,0.949,0.949}{\textcolor{black}{4.0}} & \cellcolor[rgb]{0.443,0.765,0.4}{\textcolor{black}{90.0}} & \cellcolor[rgb]{0.949,0.949,0.949}{\textcolor{black}{4.0}} \\
  Tx$_5$ & \cellcolor[rgb]{1.0,1.0,1.0}{\textcolor{black}{0.0}} & \cellcolor[rgb]{0.969,0.969,0.969}{\textcolor{black}{2.0}} & \cellcolor[rgb]{0.498,0.498,0.498}{\textcolor{black}{25.0}} & \cellcolor[rgb]{0.89,0.89,0.89}{\textcolor{black}{8.0}} & \cellcolor[rgb]{0.859,0.984,0.804}{\textcolor{black}{65.0}} \\\hline
\end{tabular}

\end{document}
```

and the following rendering 

![](test/confusionMatrix.jpg)


A default preamble is created and ca be updated (for instance to add additional packages) with 
```
push!(LatexConfusionMatrix.PREAMBULE_LATEX,"\\usepackage{myPackage}")
```
